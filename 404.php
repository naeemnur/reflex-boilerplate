<?php

/* ======================================================================
	404.php
	Template for 404 error page.
 * ====================================================================== */

get_header(); ?>
<div class="grid-col-4">
<section id="content" class="grid-two-thirds">

<article>
	<header>
		<h1><?php _e( 'Page Not Found', 'reflex' ) ?></h1>
	</header>

	<p><?php _e( 'Sorry, but the page you were looking for doesn\'t exist. It looks like this was the result of either:', 'reflex' ) ?></p>

	<ol>
		<li><?php _e( 'A mistyped address.', 'reflex' ) ?></li>
		<li><?php _e( 'An out-of-date link.', 'reflex' ) ?></li>
	</ol>

	<!-- Insert search form -->
	<?php get_search_form(); ?>

</article>

</section>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>