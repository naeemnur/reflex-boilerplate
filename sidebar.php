<div id="secondary" class="widget-area" role="complementary">
		
		<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar' ); ?>
		<?php else : ?>
		
		<div class="alert">
			<p class="no-space-bottom">
				<?php _e( 'Please activate some Widgets.', 'reflex' );  ?>
			</p>
		</div>
		
		<?php endif; ?>
</div>