<?php

/* ======================================================================
	header.php
	Template for header content.
 * ====================================================================== */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php bloginfo('description'); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php wpv_schema( 'body' ); ?>>
<div class="container">

<header id="masthead" role="banner">
	<section class="text-left space-top">
		<?php if ( get_theme_mod( 'reflex_logo' ) ) : ?>
		<div class="site-logo"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"> <img src="<?php echo get_theme_mod( 'reflex_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a> </div>
		<?php else : ?>
		<h1 id="site-name" class="text-taller no-space-bottom"> <a href="<?php echo home_url(); ?>" id="site-title" title="<?php bloginfo('name'); ?>">
			<?php bloginfo('name'); ?>
			</a></h1>
		<h2 id="site-description" class="text-tall text-muted">
			<?php bloginfo('description'); ?>
		</h2>
		<?php endif; ?>
	</section>
</header>

<?php get_template_part( 'nav-main', 'Site Navigation' ); ?>

<main id="main" class="clearfix">
