<?php

/* ======================================================================
	nav-page.php
	Template for older/newer page navigation.
 * ====================================================================== */

?>
<?php if (function_exists('reflex_basic_pagination')) { ?>
<?php reflex_basic_pagination( __('&laquo; Previuos', 'reflex') , __('Next &raquo;', "reflex") ); ?>
<?php } ?>
