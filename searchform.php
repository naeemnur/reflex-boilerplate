<?php

/* ======================================================================
	searchform.php
	Template for search form.
	`.screen-reader` class hides label when used with reflex boilerplate.
 * ====================================================================== */

?>

<form method="get" id="searchform" action="<?php echo esc_url( home_url('/') ); ?>" >
	<label class="screen-reader" for="s"><?php _e( 'Search this site:', 'reflex' ) ?></label>
	<input type="text" class="grid-fourth input-search no-space" id="s" name="s" placeholder="<?php _e( 'Search this site...', 'reflex' ) ?>" value="<?php get_search_query(); ?>">
	
	<!--<button type="submit" class="grid-fourth btn btn-search no-space" id="searchsubmit"><?php _e( 'Search', 'reflex' ) ?></button>-->
</form>