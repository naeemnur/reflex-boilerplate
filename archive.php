<?php

/* ======================================================================
	archive.php
	Template for posts by category, tag, author, date, etc.
 * ====================================================================== */

get_header(); ?>
<section id="content" class="grid-two-thirds">

<?php if (have_posts()) : ?>

	<header>
<h1>
			<?php /* If this is a category archive */ if (is_category()) { ?>
				<?php _e( 'Category:', 'reflex' ) ?> <?php single_cat_title(); ?>
			
			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<?php _e( 'Tag:', 'reflex' ) ?> <?php single_tag_title(); ?>
			
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<?php _e( 'Daily Archives:', 'reflex' ) ?> <?php echo get_the_date('F jS, Y'); ?>
			
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<?php _e( 'Monthly Archives:', 'reflex' ) ?> <?php echo get_the_date('F, Y'); ?>
			
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<?php _e( 'Yearly Archives:', 'reflex' ) ?> <?php echo get_the_date('Y'); ?>
		
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<?php _e( 'Author Archives', 'reflex' ) ?>
			
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<?php _e( 'Archives', 'reflex' ) ?>
				
			<?php } ?>
		</h1>
	</header>


	<?php while (have_posts()) : the_post(); ?>

		<article>

			<header>
				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				<aside>
					<p>
						<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_time( 'F j, Y' ) ?></time> /
						<a href="<?php comments_link(); ?>">
							<?php comments_number( __( 'Comment', 'reflex' ), __( '1 Comment', 'reflex' ), __( '% Comments', 'reflex' ) ); ?>
						</a>
						<?php edit_post_link( __( 'Edit', 'reflex' ), ' / ', '' ); ?>
					</p>
				</aside>
			</header>

			<?php the_content( __( 'Read More', 'reflex' ) ); ?>

		</article>

	<?php endwhile; ?>


	<!-- Previous/Next page navigation -->
	<?php get_template_part( 'nav-page', 'Page Navigation' ); ?>
</section>
<?php else : ?>
	<?php get_template_part( 'no-posts', 'No Posts Template' ); ?>
<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>