# Reflex Boilerplate
A lightweight boilerplate (Starter Theme) that includes just the essentials for building a WordPress theme.

## Todo
* ~~Schema.org support~~
* ~~Integrate Customizer~~
* Integrate Getting Started Page
* Integrate Quick Start Guide
* Add Offcanvas Menu option
* Comment code
* Rename function prefix
* Widgets
* Ionicons
* ~~Add page templates~~
 + ~~Full Width~~
 + ~~Archive Template~~
* ~~Register sidebar for Footer~~
* ~~Add author bio basic~~
* ~~Organize the style.css (!)~~
*  ~~grid only for shortcodes not theme layout [**Mesh grid**?] ~~
* ~~simplify layout-ing~~
* ~~Add text "**Add Menu**" to navigation when no menu is assigned~~
* ~~Add TGM Plugin Activation~~
*  ~~Shortcodes (Plugin maybe?)~~


## Changelog
* **v2.0.2 (Mar 11, 2016)**
  * added customizer
  * reorganized folders
* **v2.0.1 (Mar 11, 2016)**
  * added schema.org function
  * vita green color scheme
* **v2.0 (Mar 11, 2016)**
  * Skipped to version 2.0
  * removed customizer
  * removed TGM 
  * add title_tag support
  * simplified functions.php
  * fixed theme-check codes
* **v1.1.2 (Jan 4, 2014)**
  * added TGM Plugin support (Success!)
  * fixed some typographic stuff
* **v1.1.1 (Jan 3, 2014)**
  * added Grids[Gridism] for footer widgets(!)
  * fixed a word-wrap in layout
* **v1.1.0 (Jan 3, 2014)**
  * organized/optimized style.css
* **v1.0.9 (Dec 27, 2013)**
  * removed grid system
  * simplified layout structure
  * added clearfix
  * added semantic code
* **v1.0.8 (Dec 20, 2013)**
  * added space-top to header banner
* **v1.0.7 (Dec 17, 2013)**
  * fixed select mobile menu bug.
* **v1.0.6 (Dec 16, 2013)**
  * Redone the grid with Kraken CSS, made it more modular simpler.
* **v1.0.5 (Dec 15, 2013)**
  * added author bio box in single posts
* **v1.0.4 (Dec 14, 2013)**
  * fixed mobile menu jquery
  * some semantic html5 fix
  * lots of style fixes to navigation
* **v1.0.3 (Dec 14, 2013)**
  * added footer widgets
  * add page templates (archive & full width)
  * fixed theme-check error
* **v1.0.2 (Dec 13, 2013)**
  * added empty readme.txt
  * updated readme.md
* **v1.0.1 (Dec 13, 2013)**
  * Updated sidebar (widgets)
* **v1.0 (Dec 12, 2013)**
  * Initial release.
