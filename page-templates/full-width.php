<?php

/* ======================================================================
	Template Name: Full Width Page
	Description: Template for individual pages in full width.
 * ====================================================================== */

get_header(); ?>

<div class="grid-full">
	<section id="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
				<h1>
					<?php the_title(); ?>
				</h1>
			</header>
			<?php the_content(); ?>
			<?php edit_post_link( __( 'Edit', 'reflex' ), '<p>', '</p>' ); ?>
			<?php comments_template(); ?>
		</article>
		<?php endwhile; endif; ?>
	</section>
</div>
<?php get_footer(); ?>
