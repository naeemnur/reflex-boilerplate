<?php

/* ======================================================================
	Template Name: Archive Page
	Description: An archive page template
 * ====================================================================== */

get_header(); ?>

<div class="grid-col-4">
	<section id="content">
	
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
				<h1>
					<?php the_title(); ?>
				</h1>
			</header>
			
			<?php the_content(); ?>
			<?php edit_post_link( __( 'Edit', 'reflex' ), '<p>', '</p>' ); ?>
			
			<h6 class="headline-tags"><?php _e('Filter by Tags', 'reflex') ?></h6>
			<div class="archive-tags clearfix">
				<?php wp_tag_cloud('orderby=count&number=30'); ?>
			</div>

			<h6><?php _e('The Latest 30 Posts', 'reflex') ?></h6>
			<ul class="latest-posts-list">
				<?php wp_get_archives('type=postbypost&limit=30'); ?>
			</ul>

			<h6><?php _e('The Monthly Archive', 'reflex') ?></h6>
			<ul class="monthly-archive-list">
				<?php wp_get_archives('type=monthly'); ?>
			</ul>
			
		</article>
		
		
	</section>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
