<?php
  /*
   * layout_settings PANEL
   */

  $wp_customize->add_panel( 'wpv_pnl_custom_script', array(
    'priority' => 40,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Custom JS / CSS', 'wpvita' ),
    'description' => __( 'Description of what this panel does.', 'wpvita' ),
  ));


  /* --- [ GENERAL STYLE ] --- */
  $wp_customize->add_section( 'sec_custom_css_general', array(
    'priority' => 10,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'CSS for General', 'wpvita' ),
    'description' => '',
    'panel' => 'wpv_pnl_custom_script',
  ));

  $wp_customize->add_setting( 'custom_css_general');

  $wp_customize->add_control( 'custom_css_general', array(
    'type' => 'textarea',
    'priority' => 10,
    'section' => 'sec_custom_css_general',
    'label' => __( '', 'wpvita' ),
    'description' => '',
  ));

  /* --- [ TABLET STYLE ] --- */
  $wp_customize->add_section( 'sec_custom_css_tablet', array(
    'priority' => 20,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'CSS for Tablets', 'wpvita' ),
    'description' => 'Screen width between 768px and 985px',
    'panel' => 'wpv_pnl_custom_script',
  ));

  $wp_customize->add_setting( 'custom_css_tablet');

  $wp_customize->add_control( 'custom_css_tablet', array(
    'type' => 'textarea',
    'priority' => 10,
    'section' => 'sec_custom_css_tablet',
    'label' => __( '', 'wpvita' ),
    'description' => '',
  ));

  /* --- [ WIDE PHONE STYLE ] --- */
  $wp_customize->add_section( 'sec_custom_css_wide_phone', array(
    'priority' => 30,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'CSS for Wide Phone', 'wpvita' ),
    'description' => 'Screen width between 480px and 767px',
    'panel' => 'wpv_pnl_custom_script',
  ));

  $wp_customize->add_setting( 'custom_css_wide_phone');

  $wp_customize->add_control( 'custom_css_wide_phone', array(
    'type' => 'textarea',
    'priority' => 10,
    'section' => 'sec_custom_css_wide_phone',
    'label' => __( '', 'wpvita' ),
    'description' => '',
  ));

  /* --- [ PHONE STYLE ] --- */
  $wp_customize->add_section( 'sec_custom_css_phone', array(
    'priority' => 40,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'CSS for Phone', 'wpvita' ),
    'description' => 'Screen width up to 479px',
    'panel' => 'wpv_pnl_custom_script',
  ));

  $wp_customize->add_setting( 'custom_css_phone');

  $wp_customize->add_control( 'custom_css_phone', array(
    'type' => 'textarea',
    'priority' => 10,
    'section' => 'sec_custom_css_phone',
    'label' => __( '', 'wpvita' ),
    'description' => '',
  ));







  /* --- [ FOOTER SCRIPT ] --- */
  $wp_customize->add_section( 'pnl_general_custom_script', array(
    'priority' => 70,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Custom Script', 'wpvita' ),
    'description' => '',
    'panel' => 'wpv_pnl_custom_script'
  ));

  $wp_customize->add_setting( 'general_custom_script', array(
    'default' => '',
    'type' => 'theme_mod',
    'capability' => 'edit_theme_options',
    'transport' => '',
    'sanitize_callback' => '',
  ));

  $wp_customize->add_control( 'general_custom_script', array(
    'type' => 'textarea',
    'priority' => 10,
    'section' => 'pnl_general_custom_script',
    'label' => __( 'Script / JS', 'wpvita' ),
    'description' => '',
  ));









