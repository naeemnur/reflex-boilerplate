<?php
  /*
   * GENERAL PANEL
   */

  /* --- [ Logo / Favicon ] --- */
  $wp_customize->add_section('sec_import_export', array(
    'priority'        => 150,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Import / Export', 'wpvita' ),
    'description'     => '',
  ));

  // Export
  $wp_customize->add_setting('wpvita_export');

  $wp_customize->add_control( new WPvita_Customize_ImportExport_Control(
    $wp_customize, 'wpvita_export', array(
      'section' => 'sec_import_export',
      'settings' => 'wpvita_export',
    )
  ));


