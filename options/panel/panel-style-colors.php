<?php
  /*
   * Styling and Color Panel
   */

  $wp_customize->add_panel('wpv_panel_style_color', array(
    'priority'        => 20,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Styling and Color', 'wpvita' ),
    'description'     => __( 'Description of what this panel does.', 'wpvita' ),
  ));

  /* --- [ Google Font ] --- */
  $wp_customize->add_section('sec_google_font', array(
    'priority'        => 10,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Google Font', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_panel_style_color',
  ));
  require_once dirname(__dir__) . '/custom/controls/google_font/google-font-dropdown-custom-control.php';

  $wp_customize->add_setting( 'wpv_google_font', array(
    'default' => '',
  ));

  $wp_customize->add_control( new Google_Font_Dropdown_Custom_Control(
    $wp_customize, 'wpv_google_font',
    array(
      'priority'  => 10,
      'label'     => 'Google Font Setting',
      'section'   => 'sec_google_font',
      'settings'  => 'wpv_google_font'
  )));

  /* --- [ Background ] --- */
  $wp_customize->add_section('sec_style_bg_image', array(
    'priority'        => 20,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __('Body Background Image', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_panel_style_color',
  ));

  /* --- Background Image --- */
  $wp_customize->add_setting('site_bg_image');

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize, 'site_bg_image',
    array(
      'label'     => 'Background Image',
      'section'   => 'sec_style_bg_image',
      'settings'  => 'site_bg_image'
    )
  ));

  /* --- Background Image Repeat --- */
  $wp_customize->add_setting('site_bg_repeat', array(
    'default'        => 'no-repeat',
  ) );
  $wp_customize->add_control('site_bg_repeat', array(
    'label'   => 'Repeat:',
    'section' => 'sec_style_bg_image',
    'type'    => 'select',
    'choices' => array('no-repeat', 'repeat-x', 'repeat-y', 'repeat'),
  ));

  /* --- Background Image Repeat --- */
  $wp_customize->add_setting('site_bg_position', array(
    'default'        => 'center center',
  ) );
  $wp_customize->add_control( 'site_bg_position', array(
    'label'   => 'Repeat:',
    'section' => 'sec_style_bg_image',
    'type'    => 'select',
    'choices' => array(
                  'top left', 'top center', 'top right',
                  'center left', 'center center', 'center right',
                  'bottom left', 'bottom center', 'bottom right'),
  ));

  /* --- Background Image Scroll --- */
  $wp_customize->add_setting( 'site_bg_sroll', array(
    'default'        => 'scroll',
  ) );
  $wp_customize->add_control( 'site_bg_sroll', array(
    'label'   => 'Scroll:',
    'section' => 'sec_style_bg_image',
    'type'    => 'select',
    'choices' => array('scroll', 'fixed'),
  ));


  /* --- [ Body Color ] --- */
  $wp_customize->add_section( 'sec_style_bg_color', array(
    'priority'        => 30,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Body Color', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_panel_style_color',
  ));

  /* --- Body Background Color --- */
  $wp_customize->add_setting( 'site_bg_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_bg_color', array(
      'label'      => __( 'Body Background Color', 'wpvita' ),
      'section'    => 'sec_style_bg_color',
      'settings'   => 'site_bg_color',
    )
  ));

  /* --- Body Font Color --- */
  $wp_customize->add_setting( 'site_font_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_font_color', array(
      'label'      => __( 'Body Font Color', 'wpvita' ),
      'section'    => 'sec_style_bg_color',
      'settings'   => 'site_font_color',
    )
  ));

  /* --- [ Link Color ] --- */
  $wp_customize->add_section( 'sec_style_link_color', array(
    'priority' => 40,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Links', 'wpvita' ),
    'description' => '',
    'panel' => 'wpv_panel_style_color',
  ));

  /* --- Link Color --- */
  $wp_customize->add_setting( 'site_link_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_link_color', array(
      'label'      => __( 'Link Color', 'wpvita' ),
      'section'    => 'sec_style_link_color',
      'settings'   => 'site_link_color',
    )
  ));

  /* --- Link Hover Color --- */
  $wp_customize->add_setting( 'site_link_hover_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_link_hover_color', array(
      'label'      => __( 'Link Hover Color', 'wpvita' ),
      'section'    => 'sec_style_link_color',
      'settings'   => 'site_link_hover_color',
    )
  ));

  /* --- [ Heading Color ] --- */
  $wp_customize->add_section( 'sec_style_headlines', array(
    'priority'        => 50,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Heading Color (H1, H2...)', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_panel_style_color',
  ));

  /* --- Heading 1 Color --- */
  $wp_customize->add_setting( 'site_h1_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_h1_color', array(
      'label'      => __( 'Heading 1 &lt;h1&gt;', 'wpvita' ),
      'section'    => 'sec_style_headlines',
      'settings'   => 'site_h1_color',
    )
  ));

  /* --- Heading 2 Color --- */
  $wp_customize->add_setting( 'site_h2_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_h2_color', array(
      'label'      => __( 'Heading 2 &lt;h2&gt;', 'wpvita' ),
      'section'    => 'sec_style_headlines',
      'settings'   => 'site_h2_color',
    )
  ));

  /* --- Heading 3 Color --- */
  $wp_customize->add_setting( 'site_h3_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_h3_color', array(
      'label'      => __( 'Heading 3 &lt;h3&gt;', 'wpvita' ),
      'section'    => 'sec_style_headlines',
      'settings'   => 'site_h3_color',
    )
  ));

  /* --- Heading 4 Color --- */
  $wp_customize->add_setting( 'site_h4_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_h4_color', array(
      'label'      => __( 'Heading 4 &lt;h4&gt;', 'wpvita' ),
      'section'    => 'sec_style_headlines',
      'settings'   => 'site_h4_color',
    )
  ));

  /* --- Heading 5 Color --- */
  $wp_customize->add_setting( 'site_h5_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_h5_color', array(
      'label'      => __( 'Heading 5 &lt;h5&gt;', 'wpvita' ),
      'section'    => 'sec_style_headlines',
      'settings'   => 'site_h5_color',
    )
  ));

  /* --- Heading 6 Color --- */
  $wp_customize->add_setting( 'site_h6_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'site_h6_color', array(
      'label'      => __( 'Heading 6 &lt;h6&gt;', 'wpvita' ),
      'section'    => 'sec_style_headlines',
      'settings'   => 'site_h6_color',
    )
  ));


  /* --- [ Widget Color ] --- */
  $wp_customize->add_section( 'sec_style_widget', array(
    'priority'        => 60,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Widget Styling', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_panel_style_color',
  ));

  /* --- Widget Title Color --- */
  $wp_customize->add_setting( 'widget_title_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'widget_title_color', array(
      'label'      => __( 'Widget Title Color', 'wpvita' ),
      'section'    => 'sec_style_widget',
      'settings'   => 'widget_title_color',
    )
  ));

  /* --- Widget Title Background Color --- */
  $wp_customize->add_setting( 'widget_title_color_bg');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'widget_title_color_bg', array(
      'label'      => __( 'Widget Title Background', 'wpvita' ),
      'section'    => 'sec_style_widget',
      'settings'   => 'widget_title_color_bg',
    )
  ));


