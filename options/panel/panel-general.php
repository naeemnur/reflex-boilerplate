<?php
  /*
   * GENERAL PANEL
   */

  $wp_customize->add_panel('wpv_pnl_general', array(
    'priority'        => 10,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'General', 'wpvita' ),
    'description'     => __( 'Description of what this panel does.', 'wpvita' ),
  ));


  /* --- [ Site Title / Tagline ] --- */
  $wp_customize->add_section( 'title_tagline', array(
    'title'     => __( 'Site Title & Tagline' ),
    'priority'  => 10,
    'panel'     => 'wpv_pnl_general',
  ));

  /* --- [ Logo / Favicon ] --- */
  $wp_customize->add_section('sec_general_logo', array(
    'priority'        => 20,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Logo / Favicon', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_general',
  ));

  $wp_customize->add_setting('site_logo');

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize, 'site_logo',
    array(
      'label'     => 'Upload Logo',
      'section'   => 'sec_general_logo',
      'settings'  => 'site_logo'
    )
  ));

  $wp_customize->add_setting('site_favicon');

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize, 'site_favicon',
    array(
      'label'     => 'Upload Favicon',
      'section'   => 'sec_general_logo',
      'settings'  => 'site_favicon'
    )
  ));



  /* --- [ Google Analytics ] --- */
  $wp_customize->add_section('sec_general_analytics', array(
    'priority'        => 30,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Google Analytics', 'wpvita' ),
    'description'     => 'Add your Google Analytics or other tracking code here. This will be added into the
                          footer of your site.',
    'panel'           => 'wpv_pnl_general',
  ));

  $wp_customize->add_setting('site_analytics');

  $wp_customize->add_control( 'site_analytics', array(
    'type'        => 'textarea',
    'priority'    => 10,
    'section'     => 'sec_general_analytics',
    'label'       => __( '', 'wpvita' ),
    'description' => '',
  ));

  /* --- [ Copyright Text ] --- */
  $wp_customize->add_section('sec_general_copyright', array(
    'priority'        => 60,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Copyright Info', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_general',
  ));

  $wp_customize->add_setting('copyright_footer');

  $wp_customize->add_control( 'copyright_footer', array(
    'type'          => 'textarea',
    'priority'      => 10,
    'section'       => 'sec_general_copyright',
    'label'         => __( '', 'wpvita' ),
    'description'   => '',
  ));

  // Footer Logo
  $wp_customize->add_setting( 'footer_logo');

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize, 'footer_logo',
    array(
      'priority'   => 20,
      'label'     => 'Footer Logo',
      'section'   => 'sec_general_copyright',
      'settings'  => 'footer_logo'
    )
  ));


  /* --- [ MOBILE LOGO ] --- */
  $wp_customize->add_section( 'sec_app_icon', array(
    'priority'        => 70,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'App Icon', 'pixeldesign' ),
    'description'     => 'Upload an image for the mobile browser app. It should be 512 x 512.',
    'panel'           => 'wpv_pnl_general',
  ));

  $wp_customize->add_setting( 'app_icon');

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize, 'app_icon',
    array(
      'label'     => 'Chrome / Android / iOS',
      'section'   => 'sec_app_icon',
      'settings'  => 'app_icon'
    )
  ));

  $wp_customize->add_setting( 'app_icon_wp');

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize, 'app_icon_wp',
    array(
      'label'       => 'Windows Phone',
      'section'     => 'sec_app_icon',
      'settings'    => 'app_icon_wp',
      'description' => 'Upload an image for the Windows Phone app. It should be 144 x 144.',
    )
  ));

  /* --- [ APP COLOR ] --- */
  $wp_customize->add_section( 'sec_app_color', array(
    'priority'        => 80,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'App Color', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_general',
  ));

  $wp_customize->add_setting( 'app_color');

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'app_color', array(
      'label'      => __( 'For Chrome / Android / iOS', 'wpvita' ),
      'section'    => 'sec_app_color',
      'settings'   => 'app_color',
    )
  ));

  $wp_customize->add_setting( 'app_color_wp', array(
    'default'    => '#336699'
    ));

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize, 'app_color_wp', array(
      'label'      => __( 'For Windows Phone', 'wpvita' ),
      'section'    => 'sec_app_color',
      'settings'   => 'app_color_wp',
    )
  ));

  $wp_customize->add_section( 'static_front_page', array(
    'title'       => __( 'Static Front Page' ),
    'priority'    => 90,
    'description' => __( 'Your theme supports a static front page.', 'wpvita' ),
    'panel'       => 'wpv_pnl_general',
  ));

