<?php
  /*
   * pagination PANEL
   */

  /* --- [ SECTION ] --- */
  $wp_customize->add_section( 'sec_pagination', array(
    'priority' => 50,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Pagination', 'wpvita' ),
  ));

  $wp_customize->add_setting( 'pagination_settings', array(
    'default' => 'numeric',
  ));

  $wp_customize->add_control( 'pagination_settings', array(
    'type'    => 'radio',
    'label'   => 'Pagination',
    'section' => 'sec_pagination',
    'choices' => array( 'numeric' => 'Numeric', 'prevnext' => 'Prev / Next', 'infinite' => 'Infinite Scroll (requires Jetpack)', ),
  ));