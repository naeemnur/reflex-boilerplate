<?php
  /*
   * post-settings PANEL
   */

  $wp_customize->add_panel( 'wpv_pnl_posts', array(
    'priority' => 30,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Post Settings', 'wpvita' ),
    'description' => __( 'Description of what this panel does.', 'wpvita' ),
  ));

  /* --- [ Breadcrumbs ] --- */
  $wp_customize->add_section('sec_post_breadcrumbs', array(
    'priority'        => 10,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Breadcrumbs', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_posts',
  ));

  // Show current
  $wp_customize->add_setting('breadcrumb_show_current');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'breadcrumb_show_current', array(
      'label' => __( 'Show current', 'wpvita' ),
      'section' => 'sec_post_breadcrumbs',
      'settings' => 'breadcrumb_show_current',
      'id' => 'breadcrumb_show_current',
    )
  ));

  // Show Home Link
  $wp_customize->add_setting('breadcrumb_show_home');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'breadcrumb_show_home', array(
      'label' => __( 'Show Home Link', 'wpvita' ),
      'section' => 'sec_post_breadcrumbs',
      'settings' => 'breadcrumb_show_home',
      'id' => 'breadcrumb_show_home',
    )
  ));

  // Show Title
  $wp_customize->add_setting('breadcrumb_show_title');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'breadcrumb_show_title', array(
      'label' => __( 'Show Title', 'wpvita' ),
      'section' => 'sec_post_breadcrumbs',
      'settings' => 'breadcrumb_show_title',
      'id' => 'breadcrumb_show_title',
    )
  ));

  /* --- [ Pagenations ] --- */
  $wp_customize->add_section('sec_post_pagination', array(
    'priority'        => 20,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Pagination', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_posts',
  ));

  $wp_customize->add_setting( 'pagination_settings', array(
    'default' => 'numeric',
  ));

  $wp_customize->add_control( new WPvita_Customize_Radio_Control(
    $wp_customize, 'pagination_settings', array(
      'label' => __( 'Pagination', 'wpvita' ),
      'section' => 'sec_post_pagination',
      'settings' => 'pagination_settings',
      'id' => 'pagination_settings',
      'description' => '',
      'choices' => array( 'numeric' => 'Numeric', 'prevnext' => 'Prev / Next', 'infinite' => 'Infinite Scroll (requires Jetpack)', ),
    )
  ));


  /* --- [ Related Content ] --- */
  $wp_customize->add_section('sec_post_related', array(
    'priority'        => 30,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Related Content', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_posts',
  ));

  // Activate related content
  $wp_customize->add_setting('related_activation');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'related_activation', array(
      'label' => __( 'Activate Related content', 'wpvita' ),
      'section' => 'sec_post_related',
      'settings' => 'related_activation',
      'id' => 'related_activation',
    )
  ));

  // Display Post Image
  $wp_customize->add_setting('related_display_image', array(
    'default'    => '1'
    ));

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'related_display_image', array(
      'label' => __( 'Display Image', 'wpvita' ),
      'section' => 'sec_post_related',
      'settings' => 'related_display_image',
      'id' => 'related_display_image',
      'description' => 'Show the post thumbnail of related posts.',
    )
  ));

  // Related by
  $wp_customize->add_setting( 'related_by', array(
    'default'    => 'by_tags'
    ));

  $wp_customize->add_control( new WPvita_Customize_Radio_Control(
    $wp_customize, 'related_by', array(
      'label' => __( 'Related by:', 'wpvita' ),
      'section' => 'sec_post_related',
      'settings' => 'related_by',
      'id' => 'related_by',
      'description' => 'Show the post thumbnail of related posts.',
      'choices' => array( 'by_tags' => 'By Tags', 'by_category' => 'By Category'),
    )
  ));

  // Related title
  $wp_customize->add_setting( 'related_title');

  $wp_customize->add_control( new WPvita_Customize_TextBox_Control(
    $wp_customize, 'related_title', array(
      'label' => __( 'Title', 'wpvita' ),
      'section' => 'sec_post_related',
      'settings' => 'related_title',
      'id' => 'related_title',
      'description' => 'Title to show before related posts.',
    )
  ));

  // Related Number of posts
  $wp_customize->add_setting( 'related_no_post', array(
    'default'    => '3'
    ));

  /*$wp_customize->add_control( 'related_no_post', array(
    'type'        => 'number',
    'section'     => 'sec_post_related',
    'label'       => 'Number of posts',
    'description' => 'Number of posts you want to display.',
    'input_attrs' => array(
        'min'   => 1,
        'max'   => 10,
        'step'  => 1,
    ),));*/

    $wp_customize->add_control( new WPvita_Customize_Number_Control(
      $wp_customize, 'related_no_post', array(
        'label' => __( 'Number of posts', 'wpvita' ),
        'section' => 'sec_post_related',
        'settings' => 'related_no_post',
        'id' => 'related_no_post',
        'description' => 'Number of posts you want to display.',
      )
    ));





  /* --- [ Author Info ] --- */
  $wp_customize->add_section('sec_post_author', array(
    'priority'        => 40,
    'capability'      => 'edit_theme_options',
    'theme_supports'  => '',
    'title'           => __( 'Author Info', 'wpvita' ),
    'description'     => '',
    'panel'           => 'wpv_pnl_posts',
  ));

  // Activate Author Information
  $wp_customize->add_setting('author_activation');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'author_activation', array(
      'label' => __( 'Activate author info', 'wpvita' ),
      'section' => 'sec_post_author',
      'settings' => 'author_activation',
      'id' => 'author_activation',
    )
  ));

  // Author Name
  $wp_customize->add_setting('author_title');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'author_avatar', array(
      'label' => __( 'Author Name', 'wpvita' ),
      'section' => 'sec_post_author',
      'settings' => 'author_title',
      'id' => 'author_title',
    )
  ));

  // Author Avatar
  $wp_customize->add_setting('author_avatar');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'author_avatar', array(
      'label' => __( 'Avatar', 'wpvita' ),
      'section' => 'sec_post_author',
      'settings' => 'author_avatar',
      'id' => 'author_avatar',
    )
  ));

  // Author Biography
  $wp_customize->add_setting('author_bio');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'author_bio', array(
      'label' => __( 'Biography', 'wpvita' ),
      'section' => 'sec_post_author',
      'settings' => 'author_bio',
      'id' => 'author_bio',
    )
  ));


  // Author Link to all posts
  $wp_customize->add_setting('author_all_posts');

  $wp_customize->add_control( new WPvita_Customize_Toggle_Control(
    $wp_customize, 'author_all_posts', array(
      'label' => __( 'Link to all posts', 'wpvita' ),
      'section' => 'sec_post_author',
      'settings' => 'author_all_posts',
      'id' => 'author_all_posts',
    )
  ));
