<?php

// Custom control
require_once('custom/controls/wpvita_controls/custom-control.php');
require_once('custom/custom-style.php');
require_once('custom/custom-script.php');

function wpvita_theme_customize_style() {
  wp_enqueue_style('customize-styles', get_template_directory_uri() . '/lib/options/assets/style/wpvita-customizer.css');
  wp_enqueue_style('mdl-styles', get_template_directory_uri() . '/lib/options/assets/mdl/material.min.css');
  wp_enqueue_script('mld-script', get_template_directory_uri() . '/lib/options/assets/mdl/material.min.js');
  wp_enqueue_script('custom-script', get_template_directory_uri() . '/lib/options/assets/js/custom.js');

  wp_localize_script( 'custom-script', 'CEIl10n', array(
      'emptyImport' => __( 'Please choose a file to import.', 'customizer-export-import' )
    ));
  wp_localize_script( 'custom-script', 'wpvConfig', array(
    'customizerURL'   => admin_url( 'customize.php' ),
    'exportNonce'   => wp_create_nonce( 'wpv-exporting' )
  ));
}
add_action( 'customize_controls_enqueue_scripts', 'wpvita_theme_customize_style' );

add_action('customize_register','wpvita_theme_customize_register');
function wpvita_theme_customize_register( $wp_customize ) {

  // Remove default customizer.
  $wp_customize->remove_section("title_tagline");
  $wp_customize->remove_section("static_front_page");
  $wp_customize->remove_section("themes");

  require_once('panel/panel-general.php');
  require_once('panel/panel-style-colors.php');
  require_once('panel/panel-custom-script.php');
  require_once('panel/panel-post-settings.php');
  require_once('panel/panel-import-export.php');

  if ( isset( $_REQUEST['wpv-export'] ) ) {
    wpv_export($wp_customize);
  }
}

function wpv_export($wp_customize){
  if ( ! wp_verify_nonce( $_REQUEST['wpv-export'], 'wpv-exporting' ) ) {
      return;
    }
  require_once('custom/wpv_export.php');
}
function wpv_import($wp_customize){

}