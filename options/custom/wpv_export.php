<?php
$theme    = get_stylesheet();
$template = get_template();
$charset  = get_option( 'blog_charset' );
$mods     = get_theme_mods();
$data     = array(
              'template'  => $template,
              'mods'      => $mods ? $mods : array(),
              'options'   => array()
            );

$settings = $wp_customize->settings();

foreach ( $settings as $key => $setting ) {
  $data['options'][ $key ] = $setting->value();
}
// Plugin developers can specify additional option keys to export.
$option_keys = apply_filters( 'wpvita_export_option_keys', array() );

foreach ( $option_keys as $option_key ) {
  $option_value = get_option( $option_key );
  if ( $option_value ) {
    $data['options'][ $option_key ] = $option_value;
  }
}

header( 'Content-disposition: attachment; filename=' . $theme . '-export.dat' );
header( 'Content-Type: application/octet-stream; charset=' . $charset );

// Serialize the export data.
echo serialize( $data );

// Start the download.
die();
