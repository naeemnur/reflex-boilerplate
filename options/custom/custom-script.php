<?php
function wpv_custom_script(){
  if (get_theme_mod('general_custom_script')) { ?>
    <script type="text/javascript">
      <?php echo get_theme_mod('custom_script'); ?>
    </script>
<?php
    }
 }
add_action('wp_footer', 'wpv_custom_script'); ?>