<?php

// WPvita Toggle
if( class_exists( 'WP_Customize_Control' ) ):
  class WPvita_Customize_Toggle_Control extends WP_Customize_Control {
    public $type = 'wpv_toggle';
    public $id = '';

    public function render_content() {
    ?>
      <div class="wpvita_controls">
        <label class="mdl-switch mdl-js-switch" for="<?php echo $this->id; ?>">
          <span class="mdl-switch__label"><?php echo esc_html( $this->label ); ?></span>
          <input type="checkbox" id="<?php echo $this->id; ?>" class="mdl-switch__input" <?php $this->link(); ?>/>
        </label>
        <span style="font-size: 11px;"><?php echo esc_html( $this->description ); ?></span>
      </div>
    <?php
    }
  }
endif;

// WPvita Radio
if( class_exists( 'WP_Customize_Control' ) ):
  class WPvita_Customize_Radio_Control extends WP_Customize_Control {
    public $type = 'wpv_radio';
    public $id = '';

    public function render_content() {
      $name = '_customize-radio-' . $this->id;
      echo '<div class="wpvita_controls">';?>
    <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
      <?php

      foreach ( $this->choices as $value => $label ) :
    ?>
      <div>
      <label class="mdl-radio mdl-js-radio" for="<?php echo esc_attr( $value ); ?>">
        <input type="radio" id="<?php echo esc_attr( $value ); ?>" class="mdl-radio__button" name="<?php echo esc_attr( $name ); ?>" value="<?php echo esc_attr( $value ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?>/>
        <span class="mdl-radio__label"><?php echo esc_html( $label ); ?></span>
      </label>
    </div>
    <?php
    endforeach;
    echo '</div>';
    }
  }
endif;

// WPvita Text
if( class_exists( 'WP_Customize_Control' ) ):
  class WPvita_Customize_TextBox_Control extends WP_Customize_Control {
    public $type = 'wpv_text';
    public $id = '';

    public function render_content() {
    ?>
      <div class="wpvita_controls">
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
          <div class="mdl-textfield mdl-js-textfield textfield-demo">
            <input class="mdl-textfield__input" type="text" id="<?php echo $this->id; ?>" <?php $this->link(); ?>/>
            <label class="mdl-textfield__label" for="<?php echo $this->id; ?>"></label>
          </div>
          <span style="font-size: 11px;"><em><?php echo esc_html( $this->description ); ?></em></span>
      </div>
    <?php
    }
  }
endif;

// WPvita Number
if( class_exists( 'WP_Customize_Control' ) ):
  class WPvita_Customize_Number_Control extends WP_Customize_Control {
    public $type = 'wpv_number';
    public $id = '';

    public function render_content() {
    ?>
      <div class="wpvita_controls">
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
            <div class="mdl-textfield mdl-js-textfield textfield-demo">
              <span class="mdl-textfield__error">Input is not a number!</span>
              <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="<?php echo $this->id; ?>" <?php $this->link(); ?>/>
              <label class="mdl-textfield__label" for="<?php echo $this->id; ?>"></label>
            </div>
            <span style="font-size: 11px;"><em><?php echo esc_html( $this->description ); ?></em></span>
      </div>
    <?php
    }
  }
endif;

// WPvita Export
if( class_exists( 'WP_Customize_Control' ) ):
  class WPvita_Customize_ImportExport_Control extends WP_Customize_Control {
    public $type = 'wpv_export';
    public $id = '';

    public function render_content() {
    ?>
    <div class="wpvita_controls">
      <span class="customize-control-title"><?php _e( 'Export', 'wpvita' ); ?></span>
      <span style="font-size: 11px;"><em><?php _e( 'Click the button below to export the customization settings for this theme.', 'customizer-export-import' ); ?></em></span>
      <div>
      <div class="btn_export">
      <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--wpvita">
        Export
      </a>
      </div>
      </div>
    </div>
    <div class="wpvita_controls">
      <span class="customize-control-title"><?php _e( 'Import', 'wpvita' ); ?></span>
      <span style="font-size: 11px;"><em><?php _e( 'Upload a file to import customization settings for this theme.', 'customizer-export-import' ); ?></em></span>
      <div>
      <div class="btn_import">
        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
          Import
        </a>
      </div>
      </div>
    </div>
    <?php
    }
  }
endif;