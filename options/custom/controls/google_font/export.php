<?php

class WPvita_Custom_Export_Option extends WP_Customize_Control {
    public $type = 'wpv_export';

    public function render_content() {
        ?>
            <span class="customize-control-title">
  <?php _e( 'Export', 'wpvita' ); ?>
</span>
<span class="description customize-control-description">
  <?php _e( 'Click the button below to export the customization settings for this theme.', 'wpvita' ); ?>
</span>
<input type="button" class="button" id="wpvita-export" name="wpvita-export-button" value="<?php esc_attr_e( 'Export', 'wpvita' ); ?>" />
        <?php
    }
  }

  $wp_customize->add_setting( 'set_export' );

$wp_customize->add_control(
    new WPvita_Custom_Export_Option(
        $wp_customize,
        'wpv_export',
        array(
            'label' => 'Textarea',
            'section' => 'import_export',
            'settings' => 'set_export'
        )
    )
);

function init()
  {
    if ( current_user_can( 'edit_theme_options' ) ) {

      if ( isset( $_REQUEST['cei-export'] ) ) {
        self::_export( $wp_customize );
      }
      if ( isset( $_REQUEST['cei-import'] ) && isset( $_FILES['cei-import-file'] ) ) {
        self::_import( $wp_customize );
      }
    }
  }