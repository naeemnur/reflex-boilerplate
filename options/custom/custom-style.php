<?php

function wpv_custom_style(){
  echo '<style type="text/css">';
  ?>

  body {
    color:<?php echo get_theme_mod('color_body_font_color', '#373737'); ?>;
  }
  p {
    color:<?php echo get_theme_mod('color_p_font_color', '#373737'); ?>;
  }
  a {
    color:<?php echo get_theme_mod('color_link_color', '#373737'); ?>;
  }
  a:hover {
    color:<?php echo get_theme_mod('color_link_hover_color', '#000000'); ?>;
  }
  .button {
    background:<?php echo get_theme_mod('color_button_bg', '#16A085'); ?>;
    color:<?php echo get_theme_mod('color_button_font', '#FFFFFF'); ?>;
  }
  .button:hover {
    background:<?php echo get_theme_mod('color_button_hover_bg', '#16A085'); ?>;
    color:<?php echo get_theme_mod('color_button_hover_font', '#FFFFFF'); ?>;
  }

  <?php
    echo get_theme_mod('custom_css_general');
    if(get_theme_mod('custom_css_tablet')){
      echo '@media only screen and (max-width: 767px){';
      echo get_theme_mod('general_custom_css_tablet');
      echo '}';
    };

    if(get_theme_mod('custom_css_wide_phone')){
      echo '@media only screen and (max-width: 480px){';
      echo get_theme_mod('general_custom_css_wide_phone');
      echo '}';
    };

    if(get_theme_mod('custom_css_phone')){
      echo '@media only screen and (max-width: 340px){';
      echo get_theme_mod('sec_custom_css_phone');
      echo '}';
    };
  echo '</style>';
  }
add_action( 'wp_head', 'wpv_custom_style');