jQuery(window).load(function() {
  console.log("Working");
  jQuery('.btn_export a').click(function(event) {
    window.location.href = wpvConfig.customizerURL + '?wpv-export=' + wpvConfig.exportNonce;
  });

  jQuery('.btn_import a').click(function(event) {
    var win     = $( window ),
        body    = $( 'body' ),
        form    = $( '<form class="cei-form" method="POST" enctype="multipart/form-data"></form>' ),
        controls  = $( '.cei-import-controls' ),
        file    = $( 'input[name=cei-import-file]' ),
        message   = $( '.cei-uploading' );

      if ( '' == file.val() ) {
        alert( CEIl10n.emptyImport );
      }
      else {
        win.off( 'beforeunload' );
        body.append( form );
        form.append( controls );
        message.show();
        form.submit();
      }
  });

});