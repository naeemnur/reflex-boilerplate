<?php

/* ======================================================================
	functions.php
	For modifying and expanding core WordPress functionality.
 * ====================================================================== */


# Load Theme Options
require_once('options/customizer.php');


/*  ==========================================================================
	Add theme support
	==========================================================================  */

if ( ! isset( $content_width ) ) {
		$content_width = 600;

}

function reflex_theme_support() {

	if ( function_exists( 'add_theme_support' ) ) {

		global $wp_version;

		load_theme_textdomain( 'reflex', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size( 150, 150, true);

		add_theme_support( 'custom-background', array(
				'default-color' => 'fff',
	     ) );

		add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
		) );

		add_theme_support( 'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
		) );

		add_theme_support( 'menus' );

	}
}

add_action('after_setup_theme','reflex_theme_support');


/*  ==========================================================================
	2 - Register Wp menu
	==========================================================================  */

function reflex_register_my_menus() {

	$locations = array(
		'main-nav'		=> __( 'Main Menu', 'reflex' )
	);

	register_nav_menus( $locations );

}

add_action('init', 'reflex_register_my_menus');


function reflex_main_nav() {

	$args = array(
		'theme_location'	=> 'main-nav',
		'menu'				=> __('Main Menu', 'reflex'),
		'container'			=> 'false',
		'container_id'		=> 'access',
		'menu_class'		=> 'nav',
		'echo'				=> true,
		'items_wrap'		=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'				=> 4
	);

	wp_nav_menu( $args );

}


/*  ==========================================================================
	Register sidebar for widgets
	==========================================================================  */

function reflex_register_sidebars() {

	$args = array(
		'name'				=> __('Sidebar', 'reflex'),
		'id'				=> 'sidebar',
		'description'		=> __('Sidebar description 1', 'reflex'),
		'before_widget'		=> '<aside id="%1$s" class="hero hero-condensed widget %2$s">',
		'after_widget'		=> '</aside>',
		'before_title'		=> '<h4 class="widgettitle no-space-top">',
		'after_title'		=> '</h4>',
	);

	register_sidebar( $args );


	$args2 = array(
		'name'				=> __('Footer', 'reflex'),
		'id'				=> 'footer',
		'description'		=> __('Sidebar description 2', 'reflex'),
		'class'				=> '',
		'before_widget'		=> '<div class="unit one-quarter"><aside id="%1$s" class="hero hero-condensed widget %2$s">',
		'after_widget'		=> '</aside></div>',
		'before_title'		=> '<h4 class="widgettitle no-space-top">',
		'after_title'		=> '</h4>',
	);

	register_sidebar( $args2 );

}

add_action( 'widgets_init', 'reflex_register_sidebars' );


/*  ==========================================================================
	Register and activate css
	==========================================================================  */

function reflex_load_theme_css() {
	if (!is_admin()){

	wp_register_style( 'style', get_template_directory_uri() . '/style.css', array(), null, 'all' );
	wp_enqueue_style( 'style' );

	}

}
add_action( 'wp_enqueue_scripts', 'reflex_load_theme_css');


/*  ==========================================================================
	Register and activate js
	==========================================================================  */

function reflex_load_theme_js() {

    wp_enqueue_script( 'jquery' );

	wp_register_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js', false, null, true);
	wp_enqueue_script('scripts');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
    wp_enqueue_script( 'comment-reply' );

}
add_action('wp_enqueue_scripts', 'reflex_load_theme_js');

/*  ==========================================================================
	Schema
	==========================================================================  */

if ( ! function_exists( 'reflex_schema' ) ) :
function wpv_schema( $section, $display = true ) {
    if ( ! $section ) {
        return;
    }
    switch ( $section ) {
        case 'body':
            $microdata = 'itemtype="http://schema.org/WebPage" itemscope="itemscope"';
            break;
        case 'navigation':
            $microdata = 'itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope"';
            break;
        case 'header':
            $microdata = 'itemtype="http://schema.org/WPHeader" itemscope="itemscope"';
            break;
        case 'footer':
            $microdata = 'itemtype="http://schema.org/WPFooter" itemscope="itemscope"';
            break;
        case 'content':
            $microdata = 'itemtype="http://schema.org/Blog" itemscope="" itemprop="mainContentOfPage"';
            break;
        case 'post':
            $microdata = 'itemprop="blogPost" itemtype="http://schema.org/BlogPosting" itemscope="itemscope"';
            break;
        case 'page':
            $microdata = 'itemtype="http://schema.org/CreativeWork" itemscope="itemscope"';
            break;
        case 'code':
            $microdata = 'itemtype="http://schema.org/Code" itemscope="itemscope"';
            break;
        case 'sidebar':
            $microdata = 'itemtype="http://schema.org/WPSideBar" itemscope="itemscope"';
            break;
        case 'comment':
            $microdata = 'itemtype="http://schema.org/UserComments" itemscope="itemscope" itemprop="comment"';
            break;
        case 'entry-title':
            $microdata = 'itemprop="headline"';
            break;
        case 'entry-summary':
            $microdata = 'itemprop="description"';
            break;
        case 'entry-content':
            $microdata = 'itemprop="articleBody"';
            break;
        case 'entry-author':
            $microdata = 'itemtype="http://schema.org/Person" itemscope="itemscope" itemprop="author"';
            break;
        case 'comments-link':
            $microdata = 'itemprop="discussionURL"';
            break;
        case 'entry-url':
            $microdata = 'itemprop="url"';
            break;
        case 'cat-links':
            $microdata = 'itemprop="articleSection"';
            break;
        case 'tag-links':
            $microdata = 'itemprop="keywords"';
            break;
        case 'permalink':
            $microdata = 'itemprop="articleURL"';
            break;
        case 'comment-author':
            $microdata = 'itemtype="http://schema.org/Person" itemscope="itemscope" itemprop="creator"';
            break;
        case 'comment-time':
            $microdata = 'itemprop="commentTime"';
            break;
        case 'comment-content':
            $microdata = 'itemprop="commentText"';
            break;
        case 'comment-reply':
            $microdata = 'itemprop="replyToUrl"';
            break;
        case 'url':
            $microdata = 'itemprop="url"';
            break;
        case 'image':
            $microdata = 'itemprop="image"';
            break;
        default:
            $microdata = '';
            break;
    }
    $microdata = apply_filters( 'wpv_schema', $microdata, $section );

    if ( $display ) {
        echo $microdata;
    } else {
        return $microdata;
    }

}
endif;