<?php

/* ======================================================================
	footer.php
	Template for footer content.
 * ====================================================================== */

?>
</main>
</div>
<footer id="colophon">
	<div class="grid clearfix">
		<?php dynamic_sidebar( 'footer' ); ?>
	</div>
	<p class="text-left"><?php printf( __( 'Copyright &copy; %1$s %2$s. All rights reserved.', 'reflex' ), date( 'Y' ), get_bloginfo( 'name' ) ); ?></p>
</footer>
<?php wp_footer(); ?>
</body>
</html>