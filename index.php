<?php

/* ======================================================================
	index.php
	Template for page that displays all of your posts.
 * ====================================================================== */

get_header(); ?>

<div id="primary" class="content-area" role="main">
	<div class="primary-inner">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
				<h1><a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
					</a></h1>
				<aside>
					<p>
						<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate>
							<?php the_time( 'F j, Y' ) ?>
						</time>
						/ <a href="<?php comments_link(); ?>">
						<?php comments_number( __( 'Comment', 'reflex' ), __( '1 Comment', 'reflex' ), __( '% Comments', 'reflex' ) ); ?>
						</a> <span>
						<?php _e('Tags:','reflex');  the_tags(); ?>
						</span>
						<?php edit_post_link( __( 'Edit', 'reflex' ), ' / ', '' ); ?>
					</p>
				</aside>
			</header>
			<?php
		if ( 'option2' == get_theme_mod( 'reflex_post_content' ) ) :
			the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'reflex' ) );
		else :
			the_excerpt();
		endif;
		?>
		</article>
		<hr>
		<?php endwhile; ?>

		<!-- Previous/Next page navigation -->
		<?php get_template_part( 'nav-page', 'Page Navigation' ); ?>
	</div>
</div>
<?php else : ?>
<?php get_template_part( 'no-posts', 'No Posts Template' ); ?>
<?php endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
