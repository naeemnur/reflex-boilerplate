<?php

/* ======================================================================
	page.php
	Template for individual pages.
 * ====================================================================== */

get_header(); ?>

	<div id="primary" class="content-area" role="main">
	<div class="primary-inner">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
				<h1>
					<?php the_title(); ?>
				</h1>
			</header>
			<?php the_content(); ?>
			<?php edit_post_link( __( 'Edit', 'reflex' ), '<p>', '</p>' ); ?>
			<?php comments_template(); ?>
		</article>
		<?php endwhile; endif; ?>
	</div>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
