<?php

/* ======================================================================
	author-bio.php
	Template for showing author info in single posts.
 * ====================================================================== */

?>

<div class="author-wrap hero hero-condensed">
	<h4 class="author-headline">
		<?php _e('About the Author', 'reflex') ?>
	</h4>
	<div class="author-info"> <?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'reflex_author_bio_avatar_size', 75 ) ); ?>
		<h6><?php printf( __( 'Posted by %s', 'reflex' ), "<a href='" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . "' title='" . esc_attr( get_the_author() ) . "' rel='author'>" . get_the_author() . "</a>" ); ?></h6 >
		<p class="author-description">
			<?php the_author_meta( 'description' ); ?>
		</p>
	</div>
</div>

