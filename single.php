<?php

/* ======================================================================
	Single.php
	Template for individual blog posts.
 * ====================================================================== */

get_header(); ?>

<div id="primary" class="content-area" role="main">
	<div class="primary-inner">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<header>
				<h1><?php the_title(); ?></h1>
				<aside>
					<p>
						<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_time( 'F j, Y' ) ?></time> /
						<a href="<?php comments_link(); ?>">
							<?php comments_number( __( 'Comment', 'reflex' ), __( '1 Comment', 'reflex' ), __( '% Comments', 'reflex' ) ); ?>
						</a>
						<?php edit_post_link( __( 'Edit', 'reflex' ), ' / ', '' ); ?>
					</p>
				</aside>
			</header>

		<?php the_content(); ?>
		
			<?php
				$args = array(
					'before'			=> '<ol class="post-pagination">' . __('Pages:', 'reflex'),
					'after'				=> '</ol>',
					'link_before'		=> '',
					'link_after'		=> '',
					'next_or_number'	=> 'number',
					'nextpagelink'		=> __('Next page', 'reflex'),
					'previouspagelink'	=> __('Previous page', 'reflex'),
					'pagelink'			=> '%',
					'echo'				=> 1
				);

				wp_link_pages($args);
				?>
				
		<?php get_template_part( 'author-bio', 'Author Info Box' ); ?>
		<?php comments_template(); ?>

	</article>

<?php endwhile; endif; ?>
	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
