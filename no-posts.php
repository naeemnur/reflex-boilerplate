<?php

/* ======================================================================
	no-posts.php
	Template for when no posts are found.
 * ====================================================================== */

?>

<article>
	<header>
		<h1><?php _e( 'No posts to display', 'reflex' ) ?></h1>
	</header>
</article>