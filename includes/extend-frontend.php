<?php

/*  ==========================================================================

	1 - Tag related post
	2 - Pagination
	3 - Excerpt
	4 - Title length
	5 - Add shortcode for Search
	5 - Custom Comment Template

	==========================================================================  */


/*  ==========================================================================
	1 - Tag related post: reflex_tag_related_posts();
	==========================================================================  */

function reflex_tag_related_posts() {

	echo '<ul class="related-post">';
	global $post;
	$tags = wp_get_post_tags( $post->ID );

	if ( $tags ) {
		foreach ( $tags as $tag ) { $tag_arr .= $tag->slug . ','; }
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 5,
			'post__not_in' => array( $post->ID )
		);
		$related_posts = get_posts( $args );
		if ( $related_posts ) {
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>

<li><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
	<?php the_title(); ?>
	</a></li>
<?php endforeach; }
		else { ?>
<?php echo '<li>' . _e( 'No related post', 'reflex' ) . '</li>'; ?>
<?php }
	}
	wp_reset_query();
	echo '</ul>';

}


/*  ==========================================================================
	2 - Pagination
	==========================================================================  */

/* Numeric pagination: reflex_numeric_pagination( __('&laquo; Previuos', "reflex") , __('Next &raquo;', "reflex") ); */

function reflex_numeric_pagination( $next, $prev ) {

	global $wp_query;
	$format = '?paged=%#%';
	if ( get_option( 'permalink_structure' ) ) {
		$format = '/page/%#%';
	}
	$total_pages = $wp_query->max_num_pages;

	if ( $total_pages > 1 ) {
		$current_page = max( 1, get_query_var( 'paged' ) );
		$args = array(
			//'base'   => get_pagenum_link(1) . '%_%',
			'base'         => home_url() . '%_%',
			'format'  => $format,
			'current'  => $current_page,
			'total'   => $total_pages,
			'type'   => 'list',
			'prev_text'  => $prev,
			'next_text'  => $next,
		);
		echo paginate_links( $args );
	}

	wp_reset_query();
}


/* Basic pagination: reflex_basic_pagination( __('&laquo; Previuos', "reflex") , __('Next &raquo;', "reflex") ); */

function reflex_basic_pagination( $next, $prev ) {

	global $wp_query;
	$total_pages = $wp_query->max_num_pages;

	if ( $total_pages > 1 ) : ?>
	<nav class="pagination clearfix space-bottom">
		<?php if ( get_next_posts_link() ) { ?>
		<div class="text-left float-left">
			<?php next_posts_link( $next ); ?>
		</div>
		<?php } ?>
		<?php if ( get_previous_posts_link() ) { ?>
		<div class="text-right float-right">
			<?php previous_posts_link( $prev ); ?>
		</div>
		<?php } ?>
	</nav>
<?php endif;

	wp_reset_query();

}


/* Next & prev post pagination with thumbnails: reflex_thumb_post_pagination( __("Next article &raquo;", "reflex") , __("&laquo; Previus article", "reflex") ); */

function reflex_thumb_post_pagination( $next, $prev ) {

	global $post;
	$nextPost = get_next_post( true );
	$prevPost = get_previous_post( true );

	echo '<ul class="prev-next-thumb">';

	if ( $prevPost ) {

		$args = array(
			'posts_per_page' => 1,
			'include' => $prevPost->ID
		);

		$prevPost = get_posts( $args );

		foreach ( $prevPost as $post ) {
			setup_postdata( $post ); ?>
<li class="post-prev-thumb"> <a href="<?php the_permalink(); ?>"> <span class="title-link"><?php echo $prev ?></span>
	<?php the_post_thumbnail( 'thumbnail' ); ?>
	<span class="title-thumb">
	<?php the_title(); ?>
	</span></a> </li>
<?php wp_reset_postdata();
		}

	}

	if ( $nextPost ) {

		$args = array(
			'posts_per_page' => 1,
			'include' => $nextPost->ID
		);

		$nextPost = get_posts( $args );

		foreach ( $nextPost as $post ) {
			setup_postdata( $post ); ?>
<li class="post-next-thumb"> <a href="<?php the_permalink(); ?>"> <span class="title-link"><?php echo $next; ?></span>
	<?php the_post_thumbnail( 'thumbnail' ); ?>
	<span class="title-thumb">
	<?php the_title(); ?>
	</span></a> </li>
<?php wp_reset_postdata();
		}

	}

	echo '</ul>';

}


/*  ==========================================================================
	3 - Excerpt : reflex_excerpt( 100, __('Read more', 'reflex') );
	==========================================================================  */

/* Customize excerpt lenght */

function reflex_excerpt( $charlength, $word ) {

	global $post;
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {

		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo ' ... <a href="'. get_permalink( $post->ID ) .'" title="'. $word .' '.get_the_title( $post->ID ).'">'. $word .'</a>';

	} else {

		echo $excerpt;

	}

}


/*  ==========================================================================
	4 - Title lenght : reflex_trim_title(5, '...');
	==========================================================================  */

function reflex_pretty_wp_title( $title, $sep ) {

	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name
	$title .= get_bloginfo( 'name' );

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'reflex' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'reflex_pretty_wp_title', 10, 2 );



function reflex_trim_title( $length, $after = '' ) {

	$mytitle = get_the_title();

	if ( strlen( $mytitle ) > $length ) {
		$mytitle = substr( $mytitle, 0, $length );
		echo $mytitle . $after;
	} else {
		echo $mytitle;
	}

}

/*  ==========================================================================
	4 - Add shortcode for Search
	==========================================================================  */

function reflex_wpsearch() {
	$form = get_search_form();
	return $form;
}
add_shortcode( 'searchform', 'reflex_wpsearch' );


/*  ==========================================================================
	5 - Custom Comment Template
	==========================================================================  */

if ( ! function_exists( 'reflex_comment' ) ) :
	function reflex_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
	<article id="comment-<?php comment_ID(); ?>" class="comment <?php $comment->comment_type; ?>" itemprop="comment" itemscope itemtype="http://schema.org/UserComments">
		<footer>
			<address class="comment-author p-author author vcard hcard h-card" itemprop="creator" itemscope itemtype="http://schema.org/Person">
			<?php echo get_avatar( $comment, 50 ); ?> <?php printf( __( '%s <span class="says">says:</span>', 'reflex' ), sprintf( '<cite class="fn p-name" itemprop="name">%s</cite>', get_comment_author_link() ) ); ?>
			</address>
			<!-- .comment-author .vcard -->
			<?php if ( $comment->comment_approved == '0' ) : ?>
			<em>
			<?php _e( 'Your comment is awaiting moderation.', 'reflex' ); ?>
			</em> <br />
			<?php endif; ?>
			<div class="comment-meta commentmetadata"> <a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<time class="updated published u-updated u-published" datetime="<?php comment_time( 'c' ); ?>" itemprop="commentTime">
					<?php
		/* translators: 1: date, 2: time */
		printf( __( '%1$s at %2$s', 'reflex' ), get_comment_date(), get_comment_time() ); ?>
				</time>
				</a>
				<?php edit_comment_link( __( '(Edit)', 'reflex' ), ' ' ); ?>
			</div>
			<!-- .comment-meta .commentmetadata -->
		</footer>
		<div class="comment-content e-content p-summary p-name" itemprop="commentText name description">
			<?php comment_text(); ?>
		</div>
		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div>
		<!-- .reply -->
	</article>
	<!-- #comment-## -->

	<?php
	}
endif; // ends check for reflex_comment()


function reflex_comment_autocomplete( $fields ) {
	$fields['author'] = preg_replace( '/<input/', '<input autocomplete="nickname name" ', $fields['author'] );
	$fields['email'] = preg_replace( '/<input/', '<input autocomplete="email" ', $fields['email'] );
	$fields['url'] = preg_replace( '/<input/', '<input autocomplete="url" ', $fields['url'] );

	return $fields;
}

add_filter( 'comment_form_default_fields', 'reflex_comment_autocomplete' );
