<?php

/* ======================================================================
	nav-main.php
	Template for site navigation.

	You may wish to use `wp_nav_menu()` here.
	http://codex.wordpress.org/Function_Reference/wp_nav_menu

	Alternatively, you might hand-code your navigation
	or include a custom	function.
 * ====================================================================== */

?>

<nav id="access" class="clearfix space-bottom" role="navigation">
	<?php if( has_nav_menu( 'main-nav' ) ) { reflex_main_nav(); } 
		
		else { ?>
	<ul class="nav">
		<li> <a href="<?php echo admin_url( 'nav-menus.php' ); ?>">Add a menu</a> </li>
	</ul>
	<?php } ?>
</nav>
